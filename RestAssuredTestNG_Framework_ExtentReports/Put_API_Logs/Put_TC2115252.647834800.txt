Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Jayesh",
    "job": "SrQA"
}

Response header date is : 
Fri, 08 Mar 2024 06:22:52 GMT

Response body is : 
{"name":"Jayesh","job":"SrQA","updatedAt":"2024-03-08T06:22:52.411Z"}