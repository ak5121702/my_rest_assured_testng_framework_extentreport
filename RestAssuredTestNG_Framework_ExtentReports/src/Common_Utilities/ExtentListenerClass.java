package Common_Utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentReporter;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener {

	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
	ExtentTest test;
	
	public void reportConfigurations() {
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
		extentReport = new ExtentReports();
		extentReport.attachReporter(sparkReporter);
		
		extentReport.setSystemInfo("OS","Windows 11");
		extentReport.setSystemInfo("user","akku");
		
		sparkReporter.config().setDocumentTitle("RestAssured Extent Listener Report");
		sparkReporter.config().setReportName("This is my First Extent-Report");
		sparkReporter.config().setTheme(Theme.DARK);
		
	}
	public void onStart(ITestContext result) {
		reportConfigurations();
		System.out.println("on start method invoked");
		
	}
	public void onFinish(ITestContext result) {
		System.out.println("on Finish method invoked");
		extentReport.flush();
		
	}
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of the test method failed"+ result.getName());
	test = extentReport.createTest(result.getName());
	test.log(Status.FAIL, MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(),ExtentColor.RED ));
	}
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of the test method skipped"+result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(),ExtentColor.YELLOW));
		}
	public void onTestStart(ITestResult result) {
		System.out.println("Name of the test method started :"+ result.getName());
		
	}
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of the test method executed successfully"+result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the passed test case is: " + result.getName(),ExtentColor.GREEN));
		
	}
	public void onTestFailureButWithinSuccessPercentage(ITestResult result) {
		System.out.println("on start method invoked"+result.getName());
		
	}
}
