package Test_Packages;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestCase_DELETE_API extends RequestBody {
	@Test
	public static void executor() throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("Delete_API_Logs");
		
		String Endpoint = RequestBody.Hostname() + RequestBody.Resource_delete ();
		int statuscode=0;

		for (int i = 0; i < 5; i++) {
		Response response = API_Trigger.DELETE_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				 Endpoint);
		statuscode = response.statusCode();

		if (statuscode == 204) {
		
		Utility.evidenceDeleteFile(Utility.testLogName("Delete_TC1"), dir_name, Endpoint,
				response.getHeader("Date"),statuscode);
		validator(response);
		break;
	}
	else {
		System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
	}

}

if (statuscode!=204) {
	System.out.println("Expected status code not found even after 5 retries hence failing the test case");
	Assert.assertEquals(statuscode, 200);
}
}

		
	public static void validator(Response response) {

		
		String res_body = response.getBody().asString();
		System.out.println(response.getBody().asString());
		int statusCode = response.getStatusCode();
		System.out.println("statusCode is:" + " " + statusCode);

	    	Assert.assertEquals(statusCode, 204);
		}
	}
