package Test_Packages;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestCase_PUT_API extends RequestBody {

	@Test
		public static void executor() throws ClassNotFoundException, IOException {

			File dir_name = Utility.CreateLogDirectory("Put_API_Logs");
			String requestBody = RequestBody.req_testcase2("Put_TC2");
			String Endpoint = RequestBody.Hostname() + RequestBody.Resource_put();
			int statuscode=0;

			for (int i = 0; i < 5; i++) {
			Response response = API_Trigger.PUT_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
					requestBody, Endpoint);
			statuscode = response.statusCode();

			if (statuscode == 200) {
			
			Utility.evidenceFileCreator(Utility.testLogName("Put_TC2"), dir_name, Endpoint, requestBody,
					response.getHeader("Date"), response.getBody().asString());
			validator(response, requestBody);
			break;
		}
		else {
			System.out.println("Expected status code is not found in current iteration :" +i+ " hence retrying");
		}

	}

	if (statuscode!=200) {
		System.out.println("Expected status code not found even after 5 retries hence failing the test case");
		Assert.assertEquals(statuscode, 201);
	}

	}

		public static void validator(Response response, String requestBody) {
			ResponseBody res_body = response.getBody();
			String res_name = res_body.jsonPath().getString("name");
			String res_job = res_body.jsonPath().getString("job");
			String res_updatedAt = res_body.jsonPath().getString("updatedAt");
			res_updatedAt = res_updatedAt.substring(0, 11);

			// Set the expected results
			JsonPath jsp_req = new JsonPath(requestBody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");

			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0, 11);

			// Validate the response parameters
			Assert.assertEquals(response.statusCode(), 200);
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertEquals(res_updatedAt, expecteddate);

		}

	}
