Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vinayak",
    "job": "SrQA"
}

Response header date is : 
Thu, 07 Mar 2024 14:25:38 GMT

Response body is : 
{"name":"Vinayak","job":"SrQA","id":"302","createdAt":"2024-03-07T14:25:38.260Z"}