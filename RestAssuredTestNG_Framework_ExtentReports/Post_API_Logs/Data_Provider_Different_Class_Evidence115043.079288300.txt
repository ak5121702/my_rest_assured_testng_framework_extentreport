Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 06:20:42 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"561","createdAt":"2024-03-08T06:20:42.870Z"}