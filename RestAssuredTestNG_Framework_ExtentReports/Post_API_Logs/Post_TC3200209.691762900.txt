Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vinayak",
    "job": "SrQA"
}

Response header date is : 
Thu, 07 Mar 2024 14:32:10 GMT

Response body is : 
{"name":"Vinayak","job":"SrQA","id":"346","createdAt":"2024-03-07T14:32:09.954Z"}