Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 06:20:33 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"612","createdAt":"2024-03-08T06:20:33.754Z"}