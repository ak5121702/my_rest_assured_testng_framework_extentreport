Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 14:15:21 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"446","createdAt":"2024-03-07T14:15:21.819Z"}