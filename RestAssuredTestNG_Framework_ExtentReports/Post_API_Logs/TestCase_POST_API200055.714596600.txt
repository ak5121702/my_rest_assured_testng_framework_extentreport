Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 14:30:55 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"624","createdAt":"2024-03-07T14:30:55.734Z"}