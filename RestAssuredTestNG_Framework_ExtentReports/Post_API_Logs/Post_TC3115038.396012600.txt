Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vinayak",
    "job": "SrQA"
}

Response header date is : 
Fri, 08 Mar 2024 06:20:38 GMT

Response body is : 
{"name":"Vinayak","job":"SrQA","id":"689","createdAt":"2024-03-08T06:20:38.302Z"}