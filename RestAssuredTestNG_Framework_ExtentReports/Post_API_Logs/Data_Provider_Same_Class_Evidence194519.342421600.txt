Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Thu, 07 Mar 2024 14:15:19 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"306","createdAt":"2024-03-07T14:15:19.274Z"}