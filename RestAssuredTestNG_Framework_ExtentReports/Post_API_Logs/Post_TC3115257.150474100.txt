Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vinayak",
    "job": "SrQA"
}

Response header date is : 
Fri, 08 Mar 2024 06:22:57 GMT

Response body is : 
{"name":"Vinayak","job":"SrQA","id":"742","createdAt":"2024-03-08T06:22:57.050Z"}