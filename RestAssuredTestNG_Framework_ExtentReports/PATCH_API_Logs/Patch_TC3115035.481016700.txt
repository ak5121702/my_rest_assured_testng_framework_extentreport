Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Akki",
    "job": "SrQA"
}

Response header date is : 
Fri, 08 Mar 2024 06:20:35 GMT

Response body is : 
{"name":"Akki","job":"SrQA","updatedAt":"2024-03-08T06:20:35.317Z"}