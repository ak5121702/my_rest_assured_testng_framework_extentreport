Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Vinayak",
    "job": "Lead"
}

Response header date is : 
Thu, 07 Mar 2024 14:45:57 GMT

Response body is : 
{"name":"Vinayak","job":"Lead","updatedAt":"2024-03-07T14:45:57.785Z"}