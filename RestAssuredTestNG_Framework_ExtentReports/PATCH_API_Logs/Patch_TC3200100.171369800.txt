Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Akki",
    "job": "SrQA"
}

Response header date is : 
Thu, 07 Mar 2024 14:31:00 GMT

Response body is : 
{"name":"Akki","job":"SrQA","updatedAt":"2024-03-07T14:31:00.367Z"}