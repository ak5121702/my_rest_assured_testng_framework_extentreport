Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Hement",
    "job": "QA"
}

Response header date is : 
Thu, 07 Mar 2024 14:25:40 GMT

Response body is : 
{"name":"Hement","job":"QA","updatedAt":"2024-03-07T14:25:40.396Z"}