# My_Rest_Assured_testNG_framework_ExtentReport


#Overview:
RestAssured TestNG Framework is a powerful API testing framework built on Rest Assured. It aims to simplify and enhance the API testing process by providing a user-friendly framework. Our goal is to improve reliability and facilitate seamless integration into existing workflows, fostering collaboration among developers and testers.

Technologies Used:

[Rest Assured]: Core API testing library.
[Java]: Programming language for building the project.
[Maven]: Dependency management and build tool.

#Instructions:
Creating a project using Java RestAssured Framework.
Steps for testing:-

Create a Java project
To configure all the Rest APIs:
-Execute it
-Extract response (Utility used - RestAssured )
-Parse the response (Utility used - JsonPath )
-Validate the response (Used TestNG Library)
-Read data from excel file (Used ApachePOI)

#Features:
-Automating using different types of http methods of REST APIs such as POST, PUT, GET, PATCH and DELETE.

Create a folder add the project in that and then push it in git bash for user-friendly report.